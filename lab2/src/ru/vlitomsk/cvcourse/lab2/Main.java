package ru.vlitomsk.cvcourse.lab2;

import ru.vlitomsk.cvcourse.lab2.controller.labtasks.LabTasksLoader;
import ru.vlitomsk.cvcourse.lab2.view.MainWindow;

public class Main {
    public static void main(String[] args) {
        new MainWindow(LabTasksLoader.Load());
    }
}
