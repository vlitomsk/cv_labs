package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

import ru.vlitomsk.cvcourse.lab2.model.ObservableBufferedImage;

public class SobelTask extends LabTask {
    @Override
    public String displayedName() {
        return "Sobel magn";
    }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        int[] rgbs = image.getRGB(image.getWholeRect());
        double[][][] xys = Sobel.applyXYKernels(rgbs, image.getWidth(), image.getHeight());
        int[] resRgbs = NormalizeImg.toRgbs(NormalizeImg.normalizeMinmax(Sobel.magnitude(xys), 0., 255.));
        image.setRGB(0, 0, image.getWidth(), image.getHeight(), resRgbs);
    }
}
