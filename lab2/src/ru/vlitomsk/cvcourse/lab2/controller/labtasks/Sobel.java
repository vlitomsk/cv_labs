package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class Sobel {
    public static double[][] Y_KERNEL = new double[][]{
        {-1, -2, -1},
        {0, 0, 0},
        {1, 2, 1}
    };

    public static double[][] X_KERNEL =  new double[][] {
        {-1, 0, 1},
        {-2, 0, 2},
        {-1, 0, 1}
    };

    public static double[][][] applyXYKernels(int[] rgbs, int width, int height) {
        double[][][] res = new double[2][rgbs.length][3];
        res[0] = Convolution.convolve(rgbs, width, height, X_KERNEL);
        res[1] = Convolution.convolve(rgbs, width, height, Y_KERNEL);
        return res;
    }

    public static double[][] magnitude(double[][][] xys) {
        double[][] res = new double[xys[0].length][xys[0][0].length];
        for (int i = 0; i < xys[0].length; ++i) {
            for (int chan = 0; chan < xys[0][i].length; ++chan) {
                res[i][chan] = Math.sqrt(Math.pow(xys[0][i][chan], 2) + Math.pow(xys[1][i][chan], 2));
            }
        }
        return res;
    }
}
