package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class GaussianTask extends FilterTaskBase {
    private class GaussianDialog extends JFrame {
        public GaussianDialog() {
            setTitle("Set gaussian params");
            var pan = new JPanel();
            pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
            pan.add(jp("Sigma: ", _sigmaSpinner));
            pan.add(jp("Kernel size: ", _ksizeSpinnder));
            JButton fireBtn = new JButton("Fire!");
            pan.add(fireBtn);
            fireBtn.addActionListener(e -> {
                sigma = (Double)_sigmaSpinner.getValue();
                ksize = (Integer) _ksizeSpinnder.getValue();
                ondone.accept(true);
                this.setVisible(false);
            });
            getContentPane().add(pan, BorderLayout.CENTER);
            setSize(200, 200);
            setVisible(true);
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    ondone.accept(false);
                }
            });
        }

        private JPanel jp(String label, JSpinner spinner) {
            JPanel p = new JPanel();
            p.add(new JLabel(label));
            p.add(spinner);
            return p;
        }

        public double sigma;
        public int ksize;
        public Consumer<Boolean> ondone;

        private JSpinner _sigmaSpinner = new JSpinner(new SpinnerNumberModel(3., 0.001, 1000., 1.));
        private JSpinner _ksizeSpinnder = new JSpinner(new SpinnerNumberModel(5, 3, 1000, 2));
    }

    @Override
    protected double[][] getKernel() {
        var dlg = new GaussianDialog();
        boolean res = Threading.waitResultOrNull(cons -> {
            dlg.ondone = cons;
        });
        if (!res) {
            return new double[0][];
        } else {
            double[][] k = new double[dlg.ksize][dlg.ksize];
            int a = dlg.ksize / 2;
            for (int i = 0; i < dlg.ksize; ++i) {
                for (int j = 0; j < dlg.ksize; ++j) {
                    double rsq = Math.pow(i - a, 2) + Math.pow(j - a, 2);
                    k[i][j] = Math.exp(-rsq / (2. * dlg.sigma));
                }
            }
            return k;
        }
    }

    @Override
    public String displayedName() {
        return "Gaussian";
    }
}
