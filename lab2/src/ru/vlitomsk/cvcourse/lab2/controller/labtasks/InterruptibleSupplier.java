package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

@FunctionalInterface
public interface InterruptibleSupplier<T> {
    T get() throws InterruptedException;
}
