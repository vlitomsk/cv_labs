package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.function.Consumer;

public class GaborTask extends FilterTaskBase {
    private class GaborDialog extends JFrame {
        public GaborDialog() {
            setTitle("Set gabor params");
            var pan = new JPanel();
            pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
            pan.add(jp("Sigma: ", _sigmaSpinner));
            pan.add(jp("Theta: ", _thetaSp));
            pan.add(jp("Lambda: ", _lambdaSp));
            pan.add(jp("Gamma: ", _gammaSp));
            pan.add(jp("Psi: ", _psiSp));
            pan.add(jp("Kernel size: ", _ksizeSpinnder));
            JButton fireBtn = new JButton("Fire!");
            pan.add(fireBtn);
            fireBtn.addActionListener(e -> {
                sigma = (Double)_sigmaSpinner.getValue();
                lambda = (Double)_lambdaSp.getValue();
                gamma = (Double)_gammaSp.getValue();
                psi = (Double)_psiSp.getValue();
                theta = (Double)_thetaSp.getValue();
                ksize = (Integer) _ksizeSpinnder.getValue();
                ondone.accept(true);
                this.setVisible(false);
            });
            getContentPane().add(pan, BorderLayout.CENTER);
            setSize(200, 400);
            setVisible(true);
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    ondone.accept(false);
                }
            });
        }

        private JPanel jp(String label, JSpinner spinner) {
            JPanel p = new JPanel();
            p.add(new JLabel(label));
            p.add(spinner);
            return p;
        }

        public double sigma, theta, lambda, gamma, psi;
        public int ksize;
        public Consumer<Boolean> ondone;

        private JSpinner _sigmaSpinner = new JSpinner(new SpinnerNumberModel(3., 0.001, 1000., 1.));
        private JSpinner _thetaSp = new JSpinner(new SpinnerNumberModel(1., 0.001, 1000., .1));
        private JSpinner _lambdaSp = new JSpinner(new SpinnerNumberModel(1., 0.001, 1000., .1));
        private JSpinner _gammaSp = new JSpinner(new SpinnerNumberModel(1., 0.001, 1000., .1));
        private JSpinner _psiSp = new JSpinner(new SpinnerNumberModel(1., 0.001, 1000., .1));
        private JSpinner _ksizeSpinnder = new JSpinner(new SpinnerNumberModel(5, 3, 1000, 2));
    }

    @Override
    protected double[][] getKernel() {
        var dlg = new GaborDialog();
        boolean res = Threading.waitResultOrNull(cons -> {
            dlg.ondone = cons;
        });
        if (!res) {
            return new double[0][];
        } else {
            double[][] k = new double[dlg.ksize][dlg.ksize];
            int a = dlg.ksize / 2;
            for (int i = 0; i < dlg.ksize; ++i) {
                for (int j = 0; j < dlg.ksize; ++j) {
                    double x = i - a;
                    double y = j - a;
                    double x1 = x * Math.cos(dlg.theta) + y *Math.sin(dlg.theta);
                    double y1 = -x * Math.sin(dlg.theta) + y *Math.cos(dlg.theta);
                    double rsq = Math.pow(x1, 2) + Math.pow(y1 * dlg.gamma, 2);
                    k[i][j] = Math.exp(-rsq / 2. / Math.pow(dlg.sigma, 2)) * Math.cos(2 * Math.PI * x1 / dlg.lambda + dlg.psi);
                }
            }
            return k;
        }
    }

    @Override
    protected boolean needKernelNormalization() {
        return false;
    }

    @Override
    public String displayedName() {
        return "Gabor";
    }
}
