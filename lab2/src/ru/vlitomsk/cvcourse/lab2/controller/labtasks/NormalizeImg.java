package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class NormalizeImg {
    static class DoublePair {
        double min;
        double max;

        public DoublePair() { min = Integer.MAX_VALUE; max = Integer.MIN_VALUE;}
    }
    public static DoublePair minMaxColor(double[][] rgbs) {
        DoublePair res = new DoublePair();
        for (int i = 0; i < rgbs.length; ++i) {
            for (int j = 0; j < rgbs[i].length; ++j) {
                res.min = Math.min(res.min, rgbs[i][j]);
                res.max = Math.max(res.max, rgbs[i][j]);
            }
        }
        return res;
    }

    public static double[][] normalizeMinmax(double[][] rgbs, double dstFrom, double dstTo) {
        var minMax = minMaxColor(rgbs);
        double[][] res = new double[rgbs.length][rgbs[0].length];
        double coef = (dstTo - dstFrom) / (minMax.max - minMax.min);
        for (int i = 0; i < rgbs.length; ++i) {
            for (int j = 0 ; j < rgbs[i].length; ++j) {
                res[i][j] = (rgbs[i][j] - minMax.min) * coef + dstFrom;
            }
        }
        return res;
    }

    public static int[] toRgbs(double[][] arr) {
        int[] rgbs = new int[arr.length];
        var col = new Color(0);
        for (int i = 0; i < arr.length; ++i) {
            col.setRGB(arr[i]);
            rgbs[i] = col.getRGB();
        }
        return rgbs;
    }
}
