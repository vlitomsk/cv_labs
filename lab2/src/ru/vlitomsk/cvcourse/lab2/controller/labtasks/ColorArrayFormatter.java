package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

import ru.vlitomsk.cvcourse.lab2.model.ObservableBufferedImage;

import java.awt.*;

public class ColorArrayFormatter {
    public static String reprHSV(ObservableBufferedImage image, Rectangle rect) {
        StringBuilder sb = new StringBuilder();
        sb.append("RegionHSV 360/100/100 Rect x y w h " + rect.x + " " + rect.y + " " + rect.width + " " + rect.height + "\r\n");
        int[] rgbArr = image.getRGB(rect);
        for (int i = 0; i < rgbArr.length; ++i) {
            var color = new Color(rgbArr[i]);
            sb.append(color.hue()).append('/').append(color.saturation()).append('/').append(color.value());
            if ((i + 1) % rect.width == 0) {
                sb.append("\r\n");
            } else {
                sb.append('\t');
            }
        }
        return sb.toString();
    }

    private static String fmtDbl(double d) {
        return String.format("%.2f", d);
    }

    public static String reprLAB(ObservableBufferedImage image, Rectangle rect) {
        StringBuilder sb = new StringBuilder();
        sb.append("CIELAB Rect x y w h " + rect.x + " " + rect.y + " " + rect.width + " " + rect.height + "\r\n");
        int[] rgbArr = image.getRGB(rect);
        for (int i = 0; i < rgbArr.length; ++i) {
            var color = new Color(rgbArr[i]);
            sb
                    .append(fmtDbl(color.L()))
                    .append('/').append(fmtDbl(color.a()))
                    .append('/').append(fmtDbl(color.b()));
            if ((i + 1) % rect.width == 0) {
                sb.append("\r\n");
            } else {
                sb.append('\t');
            }
        }
        return sb.toString();
    }

    public static String reprRGB(ObservableBufferedImage image, Rectangle rect) {
        StringBuilder sb = new StringBuilder();
        sb.append("RegionRGB 255/255/255 Rect x y w h " + rect.x + " " + rect.y + " " + rect.width + " " + rect.height + "\r\n");
        int[] rgbArr = image.getRGB(rect);
        for (int i = 0; i < rgbArr.length; ++i) {
            var color = new Color(rgbArr[i]);
            sb.append(color.red()).append('/').append(color.green()).append('/').append(color.blue());
            if ((i + 1) % rect.width == 0) {
                sb.append("\r\n");
            } else {
                sb.append('\t');
            }
        }
        return sb.toString();
    }
}
