package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class IntensityDiffYTask extends FilterTaskBase {
    @Override
    protected double[][] getKernel() {
        return Sobel.Y_KERNEL;
    }

    @Override
    protected boolean needKernelNormalization() {
        return false;
    }

    @Override
    protected boolean needPostfilterNormalization() {
        return true;
    }

    @Override
    public String displayedName() {
        return "dI / dy";
    }
}
