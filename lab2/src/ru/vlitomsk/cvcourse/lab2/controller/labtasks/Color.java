package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class Color {
    public Color(int rgb) {
        setRGB(rgb);
    }

    public void setRGB(int rgb) {
        _rgb = rgb;
        _min = min(red(), min(green(), blue()));
        _max = max(red(), max(green(), blue()));
    }

    public int minRGB() { return _min; }
    public int maxRGB() { return _max; }

    public void setRGB(double[] arr) {
        if (arr.length == 1) {
            int v = Convolution.boundInt((int)arr[0], 0, 255) & 0xff;
            setRGB(v | (v << 8) | (v << 16));
        } else if (arr.length == 3) {
            int v1 =(Convolution.boundInt((int)arr[0], 0, 255)) & 0xff;
            int v2 =(Convolution.boundInt((int)arr[1], 0, 255)) & 0xff;
            int v3 =(Convolution.boundInt((int)arr[2], 0, 255)) & 0xff;
            setRGB(v1 | (v2 << 8) | (v3 << 16));
        } else {
            throw new IllegalArgumentException("arr.length must be 1 or 3");
        }
    }

    public int getRGB() {
        return _rgb;
    }

    public static Color fromHSV(short h_, short s_, short v_) {
        double r = 0, g = 0, b = 0;
        double h = h_;
        double s = s_ / 100.;
        double v = v_ / 100.;

        var i = (int) Math.floor(h / 60.);
        var f = h / 60. - i;
        var p = v * (1 - s);
        var q = v * (1 - f * s);
        var t = v * (1 - (1 - f) * s);

        switch (i % 6) {
            case 0: {
                r = v;
                g = t;
                b = p;
                break;
            }
            case 1: {
                r = q;
                g = v;
                b = p;
                break;
            }
            case 2: {
                r = p;
                g = v;
                b = t;
                break;
            }
            case 3: {
                r = p;
                g = q;
                b = v;
                break;
            }
            case 4: {
                r = t;
                g = p;
                b = v;
                break;
            }
            case 5: {
                r = v;
                g = p;
                b = q;
                break;
            }
        }
        int rr = (int) (r * 255.);
        int gg = (int) (g * 255.);
        int bb = (int) (b * 255.);
        return Color.fromRGB(rr, gg, bb);
//        return Color.fromRGB((int)r, (int)g, (int)b);
    }

    public static Color fromRGB(int r, int g, int b) {
        return new Color((b & 0xff) | ((g & 0xff) << 8) | ((r & 0xff) << 16));
    }

    private short max(short a, short b) {
        return a > b ? a : b;
    }

    private short min(short a, short b) {
        return a < b ? a : b;
    }

    public short blue() {
        return (short) (_rgb & 0xff);
    }

    public short green() {
        return (short) ((_rgb >> 8) & 0xff);
    }

    public short red() {
        return (short) ((_rgb >> 16) & 0xff);
    }

    public int rgbChan(int ch) {
        switch (ch) {
            case 0: return blue();
            case 1: return green();
            case 2: return red();
            default: throw new IllegalArgumentException("ch out of range: " + ch);
        }
    }

    public short hue() {
        if (_max == _min) {
            return 0;
        }
        double coef = 60.0 / (_max - _min);
        short r = red(), g = green(), b = blue();
        if (_max == r) {
            if (g >= b) {
                return (short) (coef * (g - b));
            } else {
                return (short) (coef * (g - b) + 360);
            }
        } else if (_max == g) {
            return (short) (coef * (b - r) + 120);
        }
        //else if (_max == b) {
        return (short) (coef * (r - g) + 240);
        //}
    }

    public short saturation() {
        if (_max == 0) {
            return 0;
        }
        return (short) (100. * (1. - (double) _min / (double) _max));
    }

    public short value() {
        return (short) (_max * 100. / 255.);
    }

    private double cc(double t) {
        t /= 255.;
        if (t > 0.04045) {
            t = Math.pow(((t + 0.055) / 1.055), 2.4);
        } else {
            t /= 12.92;
        }
        return t * 100.;
    }

    public double x() {
        return (0.412453 * cc(red())
                + 0.357580 * cc(green())
                + 0.180423 * cc(blue()));// 255.0;
    }

    public double y() {
        return (0.212671 * cc(red())
                + 0.715160 * cc(green())
                + 0.072169 * cc(blue()));// 255.0;
    }

    public double z() {
        return (0.019334 * cc(red())
                + 0.119193 * cc(green())
                + 0.950227 * cc(blue()));// 255.0;
    }

    public double L() {
        return 116.0 * labFn(y() / _Yn) - 16.;
    }

    public double a() {
        return 500.0 * (labFn(x() / _Xn) - labFn(y() / _Yn));
    }

    public double b() {
        return 200.0 * (labFn(y() / _Yn) - labFn(z() / _Zn));
    }

    private static double labFn(double t) {
        double deltasquare = 6. * 6. / 29. / 29;
        double deltacube = 6. * 6. * 6. / 29. / 29. / 29.;
        if (t > deltacube) {
            return Math.pow(t, 1. / 3);
        }
        return t / (3. * deltasquare) + 4. / 29.;
    }

    private static final double _Xn = 95.047, _Yn = 100, _Zn = 108.883;

    private int _rgb;
    private short _min, _max;
}
