package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class BlurTask extends FilterTaskBase {
    @Override
    protected double[][] getKernel() {
        return new double[][]{
                {0, 1, 0},
                {1, 1, 1},
                {0, 1, 0}
        };
    }

    @Override
    protected boolean needKernelNormalization() {
        return true;
    }

    @Override
    public String displayedName() {
        return "3x3 blur";
    }
}
