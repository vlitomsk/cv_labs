package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

import ru.vlitomsk.cvcourse.lab2.model.ObservableBufferedImage;

public abstract class FilterTaskBase extends LabTask {
    protected abstract double[][] getKernel();
    protected boolean needKernelNormalization() { return true; }
    protected boolean needPostfilterNormalization() { return false; }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        int[] rgbs = image.getRGB(image.getWholeRect());
        double[][] kernel = getKernel();
        if (needKernelNormalization()) {
            kernel = Convolution.normalizedKernel(kernel);
        }
        double[][] convolvedChans = Convolution.convolve(rgbs, image.getWidth(), image.getHeight(), kernel);
        int[] convolvedRgbs;
        if (needPostfilterNormalization()) {
            convolvedRgbs = NormalizeImg.toRgbs(NormalizeImg.normalizeMinmax(convolvedChans, 0., 255.));
        } else {
            convolvedRgbs = NormalizeImg.toRgbs(convolvedChans);
        }
        image.setRGB(0, 0, image.getWidth(), image.getHeight(), convolvedRgbs);
    }
}
