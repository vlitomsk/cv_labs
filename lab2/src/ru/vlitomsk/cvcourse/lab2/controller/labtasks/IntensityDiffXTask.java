package ru.vlitomsk.cvcourse.lab2.controller.labtasks;

public class IntensityDiffXTask extends FilterTaskBase {
    @Override
    protected double[][] getKernel() {
        return Sobel.X_KERNEL;
    }

    @Override
    protected boolean needKernelNormalization() {
        return false;
    }

    @Override
    protected boolean needPostfilterNormalization() {
        return true;
    }

    @Override
    public String displayedName() {
        return "dI / dx";
    }
}
