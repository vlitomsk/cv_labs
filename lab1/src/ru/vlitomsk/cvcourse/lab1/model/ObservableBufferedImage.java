package ru.vlitomsk.cvcourse.lab1.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class ObservableBufferedImage {
    public static ObservableBufferedImage loadFromUrl(URL input) throws IOException {
        return new ObservableBufferedImage(ImageIO.read(input));
    }

    public ObservableBufferedImage(BufferedImage source) {
        if (source == null) {
            throw new IllegalArgumentException("null source");
        }
        _source = source;
    }

    public void addOnChangeListener(ActionListener actionListener) {
        _onChangeListeners.add(actionListener);
    }

    public Rectangle getWholeRect() {
        var r = new Rectangle();
        r.x = r.y = 0;
        r.width = getWidth();
        r.height = getHeight();
        return r;
    }
    public int getWidth() { return _source.getWidth(); }

    public int getHeight() { return _source.getHeight(); }

    public int getRGB(int x, int y) { return _source.getRGB(x, y); }
    public int getRGB(Point point) { return getRGB((int)point.getX(), (int)point.getY()); }

    public int[] getRGB(int x, int y, int width, int height) {
        return _source.getRGB(x, y, width, height, null, 0, width);
    }

    public int[] getRGB(Rectangle rect) {
        return getRGB(rect.x, rect.y, rect.width, rect.height);
    }

    public void setRGB(int x, int y, int width, int height, int[] arr) {
        _source.setRGB(x, y, width, height, arr, 0, width);
        onImageChanged();
    }
    public void setRGB(int x, int y, int rgb) {
        _source.setRGB(x, y, rgb);
        onImageChanged();
    }

    public BufferedImage getBufferedImage() { return _source; }

    public ObservableBufferedImage getSubimage(int x, int y, int w, int h) {
        return getSubimage(x, y, w, h, false);
    }

    public ObservableBufferedImage getSubimage(int x, int y, int w, int h, boolean inheritListeners) {
        var result = new ObservableBufferedImage(_source.getSubimage(x, y, w, h));
        if (inheritListeners) {
            result._onChangeListeners.addAll(_onChangeListeners);
        }
        return result;
    }

    private void onImageChanged() {
        for (var listener : _onChangeListeners) {
            var actionEvent = new ActionEvent(this, 0, "");
            listener.actionPerformed(actionEvent);
        }
    }

    private final ArrayList<ActionListener> _onChangeListeners = new ArrayList<>();
    private BufferedImage _source;
}
