package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import java.awt.*;
import java.util.function.Consumer;

public interface IImageInteraction {
    void requestRegion(String comment, Consumer<Rectangle> callback);
    void requestPoint(String comment, Consumer<Point> callback);
}
