package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

public class HistogramComponent extends JComponent {
    HistogramComponent() {
        //setPreferredSize(new Dimension(Integer.MAX_VALUE, 10));
    }

    public void setBins(int[] bins) {
        _binsRef.set(bins);
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int w = getWidth();
        int h = getHeight();
        g.setColor(java.awt.Color.WHITE);
        g.fillRect(0, 0, w, h);
        var bins = _binsRef.get();
        if (bins == null || bins.length == 0) {
            return;
        }
        double max = Arrays.stream(bins).max().getAsInt();
        int wstep = w / bins.length;
        var ligherBlue = new java.awt.Color(10, 40, 230);
        for (int i = 0; i < bins.length; ++i) {
            g.setColor(i % 2 == 0 ? java.awt.Color.BLUE : ligherBlue);
            int binH = (int) (h * ((double) bins[i] / max));
            g.fillRect(i * wstep, h - binH, wstep, binH);
        }
    }

    AtomicReference<int[]> _binsRef = new AtomicReference<>();
}
