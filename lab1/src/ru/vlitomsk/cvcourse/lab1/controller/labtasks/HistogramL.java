package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

public class HistogramL extends HistogramTaskBase {
    @Override
    public String displayedName() {
        return "Histogram";
    }

    @Override
    protected double[] getValuesForHist(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        int[] rgbArr = image.getRGB(image.getWholeRect());
        double[] lvalues = new double[rgbArr.length];
        for (int i = 0; i < rgbArr.length; ++i) {
            lvalues[i] = new Color(rgbArr[i]).L();
        }
        return lvalues;
    }
}
