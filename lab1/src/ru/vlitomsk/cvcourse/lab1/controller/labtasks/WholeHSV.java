package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public class WholeHSV extends WholeSaverBase {
    @Override
    public String displayedName() {
        return "Whole HSV";
    }

    @Override
    protected String fileRepr(ObservableBufferedImage image, Rectangle rect) {
        return ColorArrayFormatter.reprHSV(image, rect);
    }
}
