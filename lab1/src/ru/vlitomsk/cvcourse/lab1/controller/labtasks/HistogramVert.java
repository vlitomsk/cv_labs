package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

public class HistogramVert extends Histogram1DTaskBase {
    @Override
    protected double[] get1DVec(int w, int h, double[] lvalues) {
        double[] res = new double[w];
        for (int i = 0; i < w; ++i) {
            for (int j = 0; j < h; ++j) {
                res[i] += lvalues[j * w + i];
            }
            res[i] /= h;
        }
        return res;
    }

    @Override
    public String displayedName() {
        return "Histogram VERT";
    }
}
