package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public class RegionRGB extends SaverBase {
    @Override
    public String displayedName() {
        return "Region RGB";
    }

    @Override
    protected Rectangle getSaveRect(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        return imageInteraction.requestRegionBlocking("");
    }

    @Override
    protected String fileRepr(ObservableBufferedImage image, Rectangle rect) {
        return ColorArrayFormatter.reprRGB(image, rect);
    }
}
