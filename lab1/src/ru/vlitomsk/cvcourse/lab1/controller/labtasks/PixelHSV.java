package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

public class PixelHSV extends LabTask {
    @Override
    public String displayedName() {
        return "Pixel HSV";
    }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        var point = imageInteraction.requestPointBlocking("");
        var color = new Color(image.getRGB(point));
        publishStatus(color.hue() + " " + color.saturation() + " " + color.value());
    }
}
