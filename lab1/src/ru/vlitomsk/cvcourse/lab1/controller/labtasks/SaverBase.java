package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class SaverBase extends LabTask {
    protected abstract Rectangle getSaveRect(ObservableBufferedImage image, ImageInteraction imageInteraction);

    protected abstract String fileRepr(ObservableBufferedImage image, Rectangle rect);

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        var rect = getSaveRect(image, imageInteraction);
        System.out.println(rect.width + " " + rect.height);
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select export target file");
        if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                bw.write(fileRepr(image, rect));
                publishStatus("Exported!");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
                publishStatus("Failed to export!");
            }
        }
    }
}
