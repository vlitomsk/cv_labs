package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public class RegionHSV extends SaverBase {
    @Override
    public String displayedName() {
        return "Region HSV";
    }

    @Override
    protected Rectangle getSaveRect(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        return imageInteraction.requestRegionBlocking("");
    }

    @Override
    protected String fileRepr(ObservableBufferedImage image, Rectangle rect) {
        return ColorArrayFormatter.reprHSV(image, rect);
    }
}
