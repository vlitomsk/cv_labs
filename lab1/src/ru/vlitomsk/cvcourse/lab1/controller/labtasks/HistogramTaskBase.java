package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

public abstract class HistogramTaskBase extends LabTask {
    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        double[] lvalues = getValuesForHist(image, imageInteraction);
        var histDlg = new HistogramDialog();
        histDlg.setOnNeedBuild(nBins -> {
            histDlg.setBins(buildHist(lvalues, 0, 100, nBins));
        });
    }

    protected abstract double[] getValuesForHist(ObservableBufferedImage image, ImageInteraction imageInteraction);

    private static int[] buildHist(double[] arr, double histMin, double histMax, int nBins) {
        int[] bins = new int[nBins];
        for (int i = 0; i < arr.length; ++i) {
            int binIndex = (int) (nBins * arr[i] / (histMax - histMin));
            binIndex = Math.min(binIndex, bins.length - 1);
            bins[binIndex]++;
        }
        return bins;
    }
}
