package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import javax.swing.*;
import java.awt.*;
import java.util.function.Consumer;

public class ChangeHSV extends LabTask {
    @Override
    public String displayedName() {
        return "Change HSV";
    }

    private class ChangeHSVDialog extends JFrame {
        public ChangeHSVDialog() {
            JPanel slidersPanel = new JPanel();
            slidersPanel.setLayout(new BoxLayout(slidersPanel, BoxLayout.PAGE_AXIS));
            JPanel hPanel = channelPanel(setThisChan -> _setH = setThisChan,
                    newVal -> _valH = newVal,
                    "H",
                    _valH, 0, 360);
            JPanel sPanel = channelPanel(setThisChan -> _setS = setThisChan,
                    newVal -> _valS = newVal,
                    "S",
                    _valS, 0, 100);
            JPanel vPanel = channelPanel(setThisChan -> _setV = setThisChan,
                    newVal -> _valV = newVal,
                    "V",
                    _valV, 0, 100);
            slidersPanel.add(hPanel);
            slidersPanel.add(sPanel);
            slidersPanel.add(vPanel);
            JButton changeBtn = new JButton("Change!");
            changeBtn.addActionListener(e -> {
                if (onBtnFired != null) {
                    onBtnFired.run();
                }
            });
            slidersPanel.add(changeBtn);
            getContentPane().add(slidersPanel, BorderLayout.CENTER);
            setSize(350, 200);
            setTitle("Change HSV");
            setVisible(true);
        }

        private JPanel channelPanel(Consumer<Boolean> setChannel, Consumer<Integer> sliderChanged, String chanName, int cur, int min, int max) {
            JPanel res = new JPanel();
            JCheckBox setCheckbox = new JCheckBox("Set " + chanName);

            JLabel valueLab = new JLabel("" + cur);
            JSlider slider = new JSlider(JSlider.HORIZONTAL, min, max, cur);
            slider.addChangeListener(l -> {
                sliderChanged.accept(slider.getValue());
                valueLab.setText("" + slider.getValue());
            });
            slider.setEnabled(false);

            setCheckbox.addChangeListener(e -> {
                setChannel.accept(setCheckbox.isSelected());
                slider.setEnabled(setCheckbox.isSelected());
            });

            res.add(setCheckbox);
            res.add(slider);
            res.add(valueLab);
            return res;
        }


        public Runnable onBtnFired;
        public boolean _setH = false, _setS = false, _setV = false;
        public int _valH = 0, _valS = 0, _valV = 0;
    }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        var dlg = new ChangeHSVDialog();
        dlg.onBtnFired = () -> {
            int[] rgbArr = image.getRGB(image.getWholeRect());
            for (int i = 0; i < rgbArr.length; ++i) {
                var col = new Color(rgbArr[i]);
                short h = col.hue();
                short s = col.saturation();
                short v = col.value();
                if (dlg._setH) {
                    h = (short) dlg._valH;
                }
                if (dlg._setS) {
                    s = (short) dlg._valS;
                }
                if (dlg._setV) {
                    v = (short) dlg._valV;
                }
                rgbArr[i] = Color.fromHSV(h, s, v).getRGB();
            }
            image.setRGB(0, 0, image.getWidth(), image.getHeight(), rgbArr);
        };
    }
}
