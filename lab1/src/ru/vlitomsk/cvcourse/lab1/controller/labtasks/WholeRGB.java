package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public class WholeRGB extends WholeSaverBase {
    @Override
    public String displayedName() {
        return "Whole RGB";
    }

    @Override
    protected String fileRepr(ObservableBufferedImage image, Rectangle rect) {
        return ColorArrayFormatter.reprRGB(image, rect);
    }
}
