package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

public class HistogramHoriz extends Histogram1DTaskBase {
    @Override
    public String displayedName() {
        return "Histogram HORIZ"; // average horizontal lines.
    }

    @Override
    protected double[] get1DVec(int w, int h, double[] lvalues) {
        double[] res = new double[h];
        for (int j = 0; j < h; ++j) {
            for (int i = 0; i < w; ++i) {
                res[j] += lvalues[j * w + i];
            }
            res[j] /= w;
        }
        return res;
    }
}
