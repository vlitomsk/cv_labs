package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import javax.swing.*;
import java.awt.*;
import java.util.function.Consumer;

public class HistogramDialog extends JFrame {
    public HistogramDialog() {
        setTitle("Histogram");
        getContentPane().add(_histComp, BorderLayout.CENTER);
        JPanel spinnerPanel = new JPanel();
        getContentPane().add(spinnerPanel, BorderLayout.SOUTH);
        JButton buildBtn = new JButton("Build!");
        buildBtn.addActionListener(e -> {
            int nBins = (Integer) _binsSpinner.getValue();
            if (_onNeedBuild != null) {
                _onNeedBuild.accept(nBins);
            }
        });
        spinnerPanel.add(new JLabel("Bins count"));
        spinnerPanel.add(_binsSpinner);
        spinnerPanel.add(buildBtn);
        setSize(500, 500);
        setVisible(true);
    }

    public void setOnNeedBuild(Consumer<Integer> onNeedBuild) {
        _onNeedBuild = onNeedBuild;
    }

    public void setBins(int[] bins) {
        _histComp.setBins(bins);
    }

    private Consumer<Integer> _onNeedBuild;
    private HistogramComponent _histComp = new HistogramComponent();
    private JSpinner _binsSpinner = new JSpinner(new SpinnerNumberModel(10, 1, 200, 1));
}
