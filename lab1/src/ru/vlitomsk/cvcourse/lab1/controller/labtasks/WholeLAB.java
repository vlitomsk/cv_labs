package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public class WholeLAB extends WholeSaverBase {
    @Override
    public String displayedName() {
        return "Whole LAB";
    }

    @Override
    protected String fileRepr(ObservableBufferedImage image, Rectangle rect) {
        return ColorArrayFormatter.reprLAB(image, rect);
    }
}
