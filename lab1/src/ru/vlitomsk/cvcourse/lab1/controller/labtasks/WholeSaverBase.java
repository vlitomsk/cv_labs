package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

import java.awt.*;

public abstract class WholeSaverBase extends SaverBase {
    @Override
    protected Rectangle getSaveRect(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        return image.getWholeRect();
    }
}
