package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

@FunctionalInterface
public interface InterruptibleSupplier<T> {
    T get() throws InterruptedException;
}
