package ru.vlitomsk.cvcourse.lab1.controller.labtasks;

import ru.vlitomsk.cvcourse.lab1.model.ObservableBufferedImage;

public abstract class Histogram1DTaskBase extends HistogramTaskBase {
    @Override
    protected double[] getValuesForHist(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        int[] rgbArr = image.getRGB(image.getWholeRect());
        double[] lvalues = new double[rgbArr.length];
        for (int i = 0; i < rgbArr.length; ++i) {
            lvalues[i] = new Color(rgbArr[i]).L();
        }
        return get1DVec(image.getWidth(), image.getHeight(), lvalues);
    }

    protected abstract double[] get1DVec(int w, int h, double[] lvalues);
}
