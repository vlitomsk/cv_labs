package ru.vlitomsk.cvcourse.lab1;

import ru.vlitomsk.cvcourse.lab1.controller.labtasks.LabTasksLoader;
import ru.vlitomsk.cvcourse.lab1.view.MainWindow;

public class Main {
    public static void main(String[] args) {
        new MainWindow(LabTasksLoader.Load());
    }
}
