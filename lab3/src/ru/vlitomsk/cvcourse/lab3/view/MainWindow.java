package ru.vlitomsk.cvcourse.lab3.view;

import ru.vlitomsk.cvcourse.lab3.controller.labtasks.IImageInteraction;
import ru.vlitomsk.cvcourse.lab3.controller.labtasks.ImageInteraction;
import ru.vlitomsk.cvcourse.lab3.controller.labtasks.LabTask;
import ru.vlitomsk.cvcourse.lab3.model.ObservableBufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class MainWindow extends JFrame implements IImageInteraction{
    public MainWindow(LabTask[] labTasks) {
        _labTasks = labTasks;
        buildJToolBar();
        getContentPane().add(_jToolbar, BorderLayout.NORTH);
        getContentPane().add(_imageScrollPane, BorderLayout.CENTER);
        _statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
        getContentPane().add(_statusBar, BorderLayout.SOUTH);
        _statusBar.setPreferredSize(new Dimension(getWidth(), 16));
        _statusBar.setLayout(new BoxLayout(_statusBar, BoxLayout.X_AXIS));
        _statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        _statusBar.add(_statusLabel);
        setTitle("CV lab3");
        setSize(800, 600);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void addToolbarComponent(JComponent component, BooleanSupplier isEnabledFunc) {
        _toolbarComponents.add(new ToolbarComponent(component, isEnabledFunc));
        _jToolbar.add(component);
    }

    private void buildJToolBar() {
        JButton openButton = new JButton("Load img");
        openButton.addActionListener(e -> {
            JFileChooser fc = new JFileChooser();
            int retval = fc.showOpenDialog(MainWindow.this);
            if (retval == JFileChooser.APPROVE_OPTION) {
                try {
                    setImage(new ObservableBufferedImage(ImageIO.read(fc.getSelectedFile())));
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(MainWindow.this, "Failed to open file");
                }
            }
        });
        addToolbarComponent(openButton, () -> true);

        for (LabTask labTask : _labTasks) {
            if (labTask == null) {
                continue;
            }
            labTask.addStatusListener(status -> SwingUtilities.invokeLater(() -> {
                _statusLabel.setText(labTask.displayedName() + ": " + status);
            }));
            JButton runTaskButton = new JButton(labTask.displayedName());
            runTaskButton.addActionListener(e -> {
                setTaskIsRunning(true);
                var taskThread = new Thread(() -> {
                    labTask.runOnImage(_image, _imageInteraction);
                    setTaskIsRunning(false);
                });
                taskThread.setDaemon(true); // detach()
                taskThread.start();
            });
            addToolbarComponent(runTaskButton, () -> _image != null && !_taskIsRunning);
        }
        updateToolbarState();
    }

    private void setImage(ObservableBufferedImage image) {
        _image = image;
        _imageView.setImage(image);
        updateToolbarState();
    }

    private void setTaskIsRunning(boolean isRunning) {
        _taskIsRunning = isRunning;
        updateToolbarState();
    }

    private void updateToolbarState() {
        for (ToolbarComponent comp : _toolbarComponents) {
            comp.updateState();
        }
    }

    private String optionalComment(String msg, String comment) {
        return (comment == null || comment.trim().isEmpty())
                ? msg
                : msg + ": " + comment;
    }

    @Override
    public void requestRegion(String comment, Consumer<Rectangle> callback) {
        _statusLabel.setText(optionalComment("Select region by dragging", comment));
        _imageView.setRectSelectionEnabled(true);
        _imageView.setRectSelectedCallback(rect -> {
            _statusLabel.setText("");
            _imageView.setRectSelectionEnabled(false);
            callback.accept(rect);
        });
    }

    @Override
    public void requestPoint(String comment, Consumer<Point> callback) {
        _statusLabel.setText(optionalComment("Select point by clicking: ", comment));
        _imageView.setPointSelectionEnabled(true);
        _imageView.setPointSelectedCallback(point -> {
            _statusLabel.setText("");
            _imageView.setPointSelectionEnabled(false);
            callback.accept(point);
        });
    }

    private final LabTask[] _labTasks;
    private boolean _taskIsRunning = false;

    private ObservableBufferedImage _image;

    private class ToolbarComponent {
        ToolbarComponent(JComponent component, BooleanSupplier isEnabledFunc) {
            _component = component;
            _isEnabledFunc = isEnabledFunc;
        }

        void updateState() {
            _component.setEnabled(_isEnabledFunc.getAsBoolean());
        }

        private final JComponent _component;
        private final BooleanSupplier _isEnabledFunc;
    }
    private final JToolBar _jToolbar = new JToolBar();
    private final ArrayList<ToolbarComponent> _toolbarComponents = new ArrayList<>();
    private final ImageView _imageView = new ImageView();
    private final JScrollPane _imageScrollPane = new JScrollPane(_imageView);
    private final JPanel _statusBar = new JPanel();
    private final JLabel _statusLabel = new JLabel();
    private final ImageInteraction _imageInteraction = new ImageInteraction(this);
}
