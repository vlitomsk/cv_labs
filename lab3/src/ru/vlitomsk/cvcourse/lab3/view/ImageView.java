package ru.vlitomsk.cvcourse.lab3.view;

import ru.vlitomsk.cvcourse.lab3.model.ObservableBufferedImage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

public class ImageView extends JPanel {
    ImageView() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (_image == null || !_pointSelectionEnabled || _pointSelectedCallback == null) {
                    return;
                }
                if (outOfBounds(e.getPoint())) { return; }
                if (_pointSelectedCallback != null) {
                    _pointSelectedCallback.accept(e.getPoint());
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (_draggingSelection && _rectSelectionEnabled && _rectSelectedCallback != null) {
                    _rectSelectedCallback.accept(getSelectedRect());
                }
                _draggingSelection = false;
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                if (_image == null || !_rectSelectionEnabled) {
                    return;
                }
                if (outOfBounds(e.getPoint())) { return; }
                if (!_draggingSelection) {
                    resetRectSelection();
                    setRectSelectionCorner(0, e.getPoint());
                }
                setRectSelectionCorner(1, e.getPoint());
                _draggingSelection = true;
            }
        });
    }

    private void resetRectSelection() {
        synchronized (_rectSelectionMonitor) {
            _selectedRectCorners[0] = null;
            _selectedRectCorners[1] = null;
        }
        repaint();
    }

    private void setRectSelectionCorner(int index, Point corner) {
        synchronized (_rectSelectionMonitor) {
            _selectedRectCorners[index] = corner;
        }
        repaint();
    }

    private boolean outOfBounds(Point point) {
        return point.x >= _image.getWidth() || point.y >= _image.getHeight() || point.x < 0 || point.y < 0;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (_image == null) { return; }
        g.drawImage(_image.getBufferedImage(), 0, 0, null);

        if (_rectSelectionEnabled) {
            var selectedRect = getSelectedRect();
            if (selectedRect != null) {
                g.setColor(Color.RED);
                g.drawRect(selectedRect.x, selectedRect.y, selectedRect.width, selectedRect.height);
            }
        }
    }

    void setImage(ObservableBufferedImage image) {
        _image = image;
        _image.addOnChangeListener(e -> repaint());
        Dimension dim = new Dimension(_image.getWidth(), _image.getHeight());
        setPreferredSize(dim);
        setSize(dim);
        repaint();
    }

    void setRectSelectionEnabled(boolean enabled) {
        _rectSelectionEnabled = enabled;
        resetRectSelection();
    }

    void setPointSelectionEnabled(boolean enabled) {
        _pointSelectionEnabled = enabled;
        _selectedPoint = null;
        if (enabled && _rectSelectionEnabled) {
            setRectSelectionEnabled(false);
        }
    }

    void setRectSelectedCallback(Consumer<Rectangle> callback) {
        _rectSelectedCallback = callback;
    }

    void setPointSelectedCallback(Consumer<Point> callback) {
        _pointSelectedCallback = callback;
    }

    private Rectangle getSelectedRect() {
        if (_selectedRectCorners[0] == null || _selectedRectCorners[1] == null) {
            return null;
        }
        var rect = new Rectangle();
        synchronized (_rectSelectionMonitor) {
            rect.x = Math.min(_selectedRectCorners[0].x, _selectedRectCorners[1].x);
            rect.y = Math.min(_selectedRectCorners[0].y, _selectedRectCorners[1].y);
            rect.width = Math.max(_selectedRectCorners[0].x, _selectedRectCorners[1].x) - rect.x;
            rect.height = Math.max(_selectedRectCorners[0].y, _selectedRectCorners[1].y) - rect.y;
        }
        return rect;
    }
    public Point getSelectedPoint() { return _selectedPoint; }

    private ObservableBufferedImage _image;
    private boolean _pointSelectionEnabled = false;
    private Point _selectedPoint;
    private boolean _rectSelectionEnabled = false;
    private final Object _rectSelectionMonitor = new Object();
    private final Point[] _selectedRectCorners = new Point[2];

    private Consumer<Rectangle> _rectSelectedCallback = null;
    private Consumer<Point> _pointSelectedCallback = null;

    private boolean _draggingSelection = false;
}
