package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import java.awt.*;
import java.util.Arrays;

public class Convolution {
    public static double[][] normalizedKernel(double[][] kernel) {
        double sum = 0.0;
        for (int i = 0; i < kernel.length; ++i) {
            for (int j = 0; j < kernel[i].length; ++j) {
                sum += kernel[i][j];
            }
        }
        double revSum = 1. / sum;
        double[][] normalized = new double[kernel.length][kernel[0].length];
        for (int i = 0; i < kernel.length; ++i) {
            for (int j = 0; j < kernel[i].length; ++j) {
                normalized[i][j] = kernel[i][j] * revSum;
            }
        }
        return normalized;
    }

    public static int boundInt(int val, int min, int max) {
        return Math.min(Math.max(val, min), max);
    }

    public static double[][] convolve(int[] rgbs, int width, int height, double[][] kernel, Point anchor, int nChans) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("width < 0 || height < 0 : width=" + width + "; height=" + height);
        }
        if (width * height != rgbs.length) {
            throw new IllegalArgumentException("width * height != rgbs.length. width=" + width + "; height=" + height
                    + "; rgbs.lenght=" + rgbs.length);
        }
        int[] res = new int[rgbs.length];
        double[][] resD = new double[rgbs.length][nChans];
        double[] chanSums = new double[nChans];
        Color color = new Color(0);
        for (int j = 0; j < height; ++j) {
            for (int i = 0; i < width; ++i) {

                Arrays.fill(chanSums, 0.);

                for (int ki = 0; ki < kernel.length; ++ki) {
                    for (int kj = 0; kj < kernel[ki].length; ++kj) {
                        double kernelCoef = kernel[ki][kj];
                        int i1 = boundInt(i + ki - anchor.x, 0, width - 1);
                        int j1 = boundInt(j + kj - anchor.y, 0, height - 1);
                        color.setRGB(rgbs[j1 * width + i1]);
                        for (int chan = 0; chan < nChans; ++chan) {
                            chanSums[chan] += kernelCoef * color.rgbChan(chan);
                        }
                    }
                }

                for (int chan = 0; chan < nChans; ++chan) {
                    resD[j * width + i][chan] = chanSums[chan];
                }
            }
        }

        return resD;
    }

    public static double[][] convolve(int[] rgbs, int width, int height, double[][] kernel) {
        return convolve(rgbs, width, height, kernel, new Point(kernel[0].length / 2 + kernel[0].length % 2 - 1, kernel.length / 2 + kernel.length % 2 - 1), 3);
    }
}
