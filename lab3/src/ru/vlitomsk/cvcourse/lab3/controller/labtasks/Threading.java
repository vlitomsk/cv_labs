package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

class Threading {
    private static <T> T waitResult(Consumer<Consumer<T>> runWithResultConsumer) throws InterruptedException {
        AtomicReference<T> result = new AtomicReference<>(null);
        AtomicBoolean hasResult = new AtomicBoolean(false);
        Object monitor = new Object();
        runWithResultConsumer.accept(t -> {
            hasResult.set(true);
            result.set(t);
            synchronized (monitor) {
                monitor.notifyAll();
            }
        });
        synchronized (monitor) {
            while (!hasResult.get()) {
                monitor.wait();
            }
        }
        return result.get();
    }

    private static <T> T resultOrNull(InterruptibleSupplier<T> resultGetter) {
        try {
            return resultGetter.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    static <T> T waitResultOrNull(Consumer<Consumer<T>> runWithResultConsumer) {
        return resultOrNull(() -> waitResult(runWithResultConsumer));
    }
}
