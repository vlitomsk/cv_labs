package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

@FunctionalInterface
public interface InterruptibleSupplier<T> {
    T get() throws InterruptedException;
}
