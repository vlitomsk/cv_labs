package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import java.awt.*;
import java.util.function.Consumer;

public class ImageInteraction implements IImageInteraction {
    public ImageInteraction(IImageInteraction proxied) {
        _proxied = proxied;
    }

    @Override
    public void requestRegion(String comment, Consumer<Rectangle> callback) {
        _proxied.requestRegion(comment, callback);
    }

    public Rectangle requestRegionBlocking(String comment) {
        return Threading.waitResultOrNull(cons -> requestRegion(comment, cons));
    }

    @Override
    public void requestPoint(String comment, Consumer<Point> callback) {
        _proxied.requestPoint(comment, callback);
    }

    public Point requestPointBlocking(String comment) {
        return Threading.waitResultOrNull(cons -> requestPoint(comment, cons));
    }

    private final IImageInteraction _proxied;
}
