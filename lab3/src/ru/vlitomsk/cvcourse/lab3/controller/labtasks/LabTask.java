package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import ru.vlitomsk.cvcourse.lab3.model.ObservableBufferedImage;

import java.util.ArrayList;
import java.util.function.Consumer;

public abstract class LabTask {
    public abstract String displayedName();

    // called in separate thread.
    public abstract void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction);

    public void addStatusListener(Consumer<String> status) {
        _statusListeners.add(status);
    }

    protected void publishStatus(String status) {
        for (var listener : _statusListeners) {
            listener.accept(status);
        }
    }

    private final ArrayList<Consumer<String>> _statusListeners = new ArrayList<>();
}
