package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import ru.vlitomsk.cvcourse.lab3.model.ObservableBufferedImage;

import java.awt.*;
import java.util.*;

public class SplitMergeTask extends LabTask {
    @Override
    public String displayedName() {
        return "Split merge";
    }

    ArrayList<Rectangle> _regionList = new ArrayList<>();
    boolean[] used;
    Stack<Rectangle> _processList = new Stack<>();

    public boolean IsUniformRegion(ObservableBufferedImage image, Rectangle rect, double sigmaThresh) {
        return RegionSigma(image, rect) < sigmaThresh;
    }

    public boolean IsUniformRegion(ObservableBufferedImage image, Collection<Rectangle> rects, double sigmaThresh) {
        return RegionSigma(image, rects) < sigmaThresh;
    }

    public double RegionSigma(ObservableBufferedImage image, Rectangle rect) {
        var rects = new ArrayList<Rectangle>();
        rects.add(rect);
        return RegionSigma(image, rects);
    }

    public double RegionSigma(ObservableBufferedImage image, Collection<Rectangle> rects) {
        int[][] rgbsArr = new int[rects.size()][];
        int cnt = 0;
        int avgDivisor = 0;
        for (var rect : rects) {
            rgbsArr[cnt++] = image.getRGB(rect);
            avgDivisor += rgbsArr[cnt - 1].length;
        }

        double Lavg = 0, aavg = 0, bavg = 0;
        cnt = 0;
        double rd = 1. / avgDivisor;
        var col = new Color(0);
        for (var rect : rects) {
            int[] rgbs = rgbsArr[cnt++];
            for (int i = 0; i < rgbs.length; ++i) {
                col.setRGB(rgbs[i]);
                Lavg += col.L() * rd;
                aavg += col.a() * rd;
                bavg += col.b() * rd;
            }
        }

        double sigma = 0;
        cnt = 0;
        for (var rect : rects) {
            int[] rgbs = rgbsArr[cnt++];
            for (int i = 0; i < rgbs.length; ++i) {
                col.setRGB(rgbs[i]);
                //double diff=Math.pow(Math.pow(Lavg - col.red(), 2) + Math.pow(aavg - col.green(), 2) + Math.pow(bavg - col.blue(), 2), 0.5);
                double diff = CIEDE2000.calculateDeltaE(Lavg, aavg, bavg, col.L(), col.a(), col.b());
                sigma += Math.pow(diff, 2) * rd;
            }
        }

        System.out.println("Sigma " + sigma);
        return sigma;
    }


    public boolean CanSplit(Rectangle rect) {
        return rect.width >= 10 && rect.height >= 10;
    }

    public void AddSplitted(Rectangle rect, Stack<Rectangle> dst) {
        if (!CanSplit(rect)) { throw new RuntimeException("assertion failed : !cansplit()"); }
        int wx = rect.width /2;
        int wy = rect.height /2;

        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < 2; ++j) {
                var r = new Rectangle(i == 0 ? rect.x : rect.x + wx,
                                      j == 0 ? rect.y : rect.y + wy,
                                      rect.width - wx, rect.height - wy);
                dst.push(r);
            }
        }
    }

    static boolean IsNeigh(Rectangle a, Rectangle b) {
        for (int i = 0; i < 2; ++i) {
            int at0 = i == 0 ? a.x : a.y;
            int at1 = i == 0 ? a.x + a.width - 1 : a.y + a.height - 1;

            int bt0 = i == 0 ? b.x : b.y;
            int bt1 = i == 0 ? b.x + b.width - 1 : b.y + b.height - 1;

            int as0 = i == 1 ? a.x : a.y;
            int as1 = i == 1 ? a.x + a.width - 1 : a.y + a.height - 1;

            int bs0 = i == 1 ? b.x : b.y;
            int bs1 = i == 1 ? b.x + b.width - 1 : b.y + b.height - 1;

            if ((bt1 + 1 == at0 || at1 + 1 == bt0) && isect1d(as0, as1, bs0, bs1)) {
                return true;
            }
        }
        return false;
    }

    static boolean IsNeigh(Collection<Rectangle> rects, Rectangle r) {
        return rects.stream().anyMatch(rr -> IsNeigh(rr, r));
    }

    static boolean isect1d(int a1_, int a2_, int b1_, int b2_) {
        int a1 = Math.min(a1_, a2_);
        int a2 = Math.max(a1_, a2_);
        int b1 = Math.min(b1_, b2_);
        int b2 = Math.max(b1_, b2_);

        return a2 >= b1 && a2 <= b2 ||
                b1 >= a1 && b1 <= a2;
    }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        var r1 = new Rectangle(0, 0, 1, 1);
        var r2 = new Rectangle(1, 1, 1, 1);
        var r3 = new Rectangle(1, 0, 1, 1);
        var r4 = new Rectangle(0, 1, 1, 1);

        System.out.println(IsNeigh(r1, r2)); // false
        System.out.println(IsNeigh(r1, r3)); // true
        System.out.println(IsNeigh(r1, r4)); // true
        System.out.println(IsNeigh(r4, r3)); // false
        System.out.println(IsNeigh(r3, r2)); // true

        _regionList.clear();
        _processList.clear();
        final double sigmaThresh = 140;
        _processList.push(image.getWholeRect());
        while (!_processList.empty()) {
            System.out.println("Process list: " + _processList.size());
            var rect = _processList.pop();
            if (!IsUniformRegion(image, rect, sigmaThresh) && CanSplit(rect)) {
                AddSplitted(rect, _processList);
                System.out.println("---");
            } else {
                System.out.println("+++");
                _regionList.add(rect);
            }
        }

        System.out.println("Nregions: " + _regionList.size());

        var used = new boolean[_regionList.size()];
        var segments = new ArrayList<ArrayList<Rectangle>>();

        for (int i = 0; i < _regionList.size(); ++i) {
            /*var c = new ArrayList<Rectangle>();
            c.add(_regionList.get(i));
            segments.add(c);
            continue;*/
            if (used[i]) { continue; }
            System.out.println("Regions left: " + (_regionList.size() - i - 1));
            var curSeg = new ArrayList<Rectangle>();
            used[i] = true;
            curSeg.add(_regionList.get(i));
            for (int j = i + 1; j < _regionList.size(); ++j) {
                curSeg.add(_regionList.get(j));
                used[j] = true;
                if (!IsNeigh(curSeg, _regionList.get(j)) || !IsUniformRegion(image, curSeg, sigmaThresh)) {
                    curSeg.remove(curSeg.size() - 1);
                    used[j] = false;
                }
            }

            segments.add(curSeg);
        }

        Random rnd = new Random();
        System.out.println("n segments: " + segments.size());
        var col = new Color(0);
        for (var segment : segments) {
            int totArea=0;
            int totR = 0, totG=0, totB = 0;
            for (var rect : segment) {
                totArea += rect.width*rect.height;
                int[] ar = image.getRGB(rect);
                for (int ccq : ar) {
                    col.setRGB(ccq);
                    totR += col.red();
                    totG += col.green();
                    totB += col.blue();
                }
            }
            var ccc = Color.fromRGB(totR / totArea, totG / totArea, totB / totArea);
            int color = ccc.getRGB();
            for (var rect : segment) {
                int[] solid = new int[rect.width * rect.height];
                Arrays.fill(solid, color);
                image.setRGB(rect.x, rect.y, rect.width, rect.height, solid);
            }
        }
    }
}
