package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

public class LabTasksLoader {
    public static LabTask[] Load() {
        Class[] classes = new Class[] {
            SplitMergeTask.class, MeanshiftTask.class
        };

        LabTask[] result = new LabTask[classes.length];
        for (int i = 0; i < classes.length; ++i) {
            if (!LabTask.class.isAssignableFrom(classes[i])) {
                System.err.println("Not LabTask inheritor: " + classes[i].getName());
                continue;
            }
            try {
                result[i] = (LabTask) classes[i].getDeclaredConstructor().newInstance();
            } catch (NoSuchMethodException noSuchMethod) {
                System.err.println("Class " + classes[i].getName() + " doesn't have default constructor, ignoring.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
