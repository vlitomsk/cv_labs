package ru.vlitomsk.cvcourse.lab3.controller.labtasks;

import ru.vlitomsk.cvcourse.lab3.model.ObservableBufferedImage;

public class MeanshiftTask extends LabTask{

    @Override
    public String displayedName() {
        return "Meanshift";
    }


    static int LAB2RGB(int L, int a, int b) {
        double X, Y, Z, fX, fY, fZ;
        int RR, GG, BB;

        fY = Math.pow((L + 16.0) / 116.0, 3.0);
        if (fY < 0.008856)
            fY = L / 903.3;
        Y = fY;

        if (fY > 0.008856)
            fY = Math.pow(fY, 1.0/3.0);
        else
            fY = 7.787 * fY + 16.0/116.0;

        fX = a / 500.0 + fY;
        if (fX > 0.206893)
            X = Math.pow(fX, 3.0);
        else
            X = (fX - 16.0/116.0) / 7.787;

        fZ = fY - b /200.0;
        if (fZ > 0.206893)
            Z = Math.pow(fZ, 3.0);
        else
            Z = (fZ - 16.0/116.0) / 7.787;

        X *= (0.950456 * 255);
        Y *=             255;
        Z *= (1.088754 * 255);

        RR =  (int)(3.240479*X - 1.537150*Y - 0.498535*Z + 0.5);
        GG = (int)(-0.969256*X + 1.875992*Y + 0.041556*Z + 0.5);
        BB =  (int)(0.055648*X - 0.204043*Y + 1.057311*Z + 0.5);

        int R = (RR < 0 ? 0 : RR > 255 ? 255 : RR);
        int G = (GG < 0 ? 0 : GG > 255 ? 255 : GG);
        int B = (BB < 0 ? 0 : BB > 255 ? 255 : BB);

        return (B | (G << 8) | (R << 16));

        //printf("Lab=(%f,%f,%f) ==> RGB(%f,%f,%f)\n",L,a,b,*R,*G,*B);
    }

    @Override
    public void runOnImage(ObservableBufferedImage image, ImageInteraction imageInteraction) {
        Point5D PtCur = new Point5D();
        Point5D PtPrev = new Point5D();
        Point5D PtSum = new Point5D();
        Point5D Pt = new Point5D();
        int ROWS = image.getHeight();
        int COLS = image.getWidth();
        int Left;
        int Right;
        int Top;
        int Bottom;
        int NumPts;					// number of points in a hypersphere
        int step;

        int[] rgbs = image.getRGB(image.getWholeRect());
        var col = new Color(0);

        final int hs = 5;
        final double hr = 20.;

        final double MS_MEAN_SHIFT_TOL_COLOR = 0.3;
        final double MS_MEAN_SHIFT_TOL_SPATIAL = 0.3
        final int MS_MAX_NUM_CONVERGENCE_STEPS = 5;
        int dxdy[][] = {{-1,-1},{-1,0},{-1,1},{0,-1},{0,1},{1,-1},{1,0},{1,1}};

        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS; j++){
                Left = (j - hs) > 0 ? (j - hs) : 0;
                Right = (j + hs) < COLS ? (j + hs) : COLS;
                Top = (i - hs) > 0 ? (i - hs) : 0;
                Bottom = (i + hs) < ROWS ? (i + hs) : ROWS;
                col.setRGB(rgbs[i * COLS] + j);;
                PtCur.MSPOint5DSet(i, j, col.L(), col.a(), col.b());
                step = 0;
                do{

                    PtPrev.MSPoint5DCopy(PtCur);
                    PtSum.MSPOint5DSet(0, 0, 0, 0, 0);
                    NumPts = 0;
                    for(int hx = Top; hx < Bottom; hx++){
                        for(int hy = Left; hy < Right; hy++){

                            col.setRGB(rgbs[hx*COLS + hy]);
                            Pt.MSPOint5DSet(hx, hy, col.L(), col.a(), col.b());

                            if(Pt.MSPoint5DColorDistance(PtCur) < hr){
                                PtSum.MSPoint5DAccum(Pt);
                                NumPts++;
                            }
                        }
                    }
                    PtSum.MSPoint5DScale(1.0 / NumPts);
                    PtCur.MSPoint5DCopy(PtSum);
                    step++;
                }while((PtCur.MSPoint5DColorDistance(PtPrev) > MS_MEAN_SHIFT_TOL_COLOR) && (PtCur.MSPoint5DSpatialDistance(PtPrev) > MS_MEAN_SHIFT_TOL_SPATIAL) && (step < MS_MAX_NUM_CONVERGENCE_STEPS));


                rgbs[i*COLS+j] = LAB2RGB((int)PtCur.l, (int)PtCur.a, (int)PtCur.b);
            }
        }

        int RegionNumber = 0;			// Reigon number
        int label = -1;					// Label number
        float *Mode = new float [ROWS * COLS * 3];					// Store the Lab color of each region
        int *MemberModeCount = new int [ROWS * COLS];				// Store the number of each region
        memset(MemberModeCount, 0, ROWS * COLS * sizeof(int));		// Initialize the MemberModeCount
        split(Img, IMGChannels);
        // Label for each point
        int **Labels = new int *[ROWS];
        for(int i = 0; i < ROWS; i++)
            Labels[i] = new int [COLS];

        // Initialization
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS; j++){
                Labels[i][j] = -1;
            }
        }

        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS;j ++){
                // If the point is not being labeled
                if(Labels[i][j] < 0){
                    Labels[i][j] = ++label;		// Give it a new label number
                    // Get the point
                    PtCur.MSPOint5DSet(i, j, (float)IMGChannels[0].at<uchar>(i, j), (float)IMGChannels[1].at<uchar>(i, j), (float)IMGChannels[2].at<uchar>(i, j));
                    PtCur.PointLab();

                    // Store each value of Lab
                    Mode[label * 3 + 0] = PtCur.l;
                    Mode[label * 3 + 1] = PtCur.a;
                    Mode[label * 3 + 2] = PtCur.b;

                    // Region Growing 8 Neighbours
                    vector<Point5D> NeighbourPoints;
                    NeighbourPoints.push_back(PtCur);
                    while(!NeighbourPoints.empty()){
                        Pt = NeighbourPoints.back();
                        NeighbourPoints.pop_back();

                        // Get 8 neighbours
                        for(int k = 0; k < 8; k++){
                            int hx = Pt.x + dxdy[k][0];
                            int hy = Pt.y + dxdy[k][1];
                            if((hx > 0) && (hy > 0) && (hx < ROWS) && (hy < COLS) && (Labels[hx][hy] < 0)){
                                Point5D P;
                                P.MSPOint5DSet(hx, hy, (float)IMGChannels[0].at<uchar>(hx, hy), (float)IMGChannels[1].at<uchar>(hx, hy), (float)IMGChannels[2].at<uchar>(hx, hy));
                                P.PointLab();

                                // Check the color
                                if(PtCur.MSPoint5DColorDistance(P) < hr){
                                    // Satisfied the color bandwidth
                                    Labels[hx][hy] = label;				// Give the same label
                                    NeighbourPoints.push_back(P);		// Push it into stack
                                    MemberModeCount[label]++;			// This region number plus one
                                    // Sum all color in same region
                                    Mode[label * 3 + 0] += P.l;
                                    Mode[label * 3 + 1] += P.a;
                                    Mode[label * 3 + 2] += P.b;
                                }
                            }
                        }
                    }
                    MemberModeCount[label]++;							// Count the point itself
                    Mode[label * 3 + 0] /= MemberModeCount[label];		// Get average color
                    Mode[label * 3 + 1] /= MemberModeCount[label];
                    Mode[label * 3 + 2] /= MemberModeCount[label];
                }
            }
        }
        RegionNumber = label + 1;										// Get region number

        // Get result image from Mode array
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLS; j++){
                label = Labels[i][j];
                float l = Mode[label * 3 + 0];
                float a = Mode[label * 3 + 1];
                float b = Mode[label * 3 + 2];
                Point5D Pixel;
                Pixel.MSPOint5DSet(i, j, l, a, b);
                Pixel.PointRGB();
//			Pixel.Print();
                Img.at<Vec3b>(i, j) = Vec3b(Pixel.l, Pixel.a, Pixel.b);
            }
        }
    }

    class Point5D {
        double x, y, l, a, b;
        public void MSPoint5DAccum(Point5D pt) {
            x += pt.x;
            y += pt.y;
            l += pt.l;
            a += pt.a;
            b += pt.b;
        }
        public Point5D clone() {
            Point5D res = new Point5D();
            res.x = x;
            res.y = y;
            res.l = l;
            res.a = a;
            res.b = b;
            return res;
        }

        void MSPOint5DSet(double x, double y, double l, double a , double b) {
            this.x = x;
            this.y = y;
            this.l = l;
            this.a = a;
            this.b =b ;
        }

        void MSPoint5DCopy(Point5D p) {
            this.x = p.x;
            y=p.y;
            l=p.l;
            a=p.a;
            b=p.b;
        }

        double MSPoint5DColorDistance(Point5D p) {
            return Math.sqrt(Math.pow(p.l-l,2) + Math.pow(p.a-a, 2) + Math.pow(p.b-b,2));;
        }

        double MSPoint5DSpatialDistance(Point5D p) {
            return Math.sqrt(Math.pow(p.x-x,2)+Math.pow(p.y-y,2));
        }
        public void MSPoint5DScale(double s) {
            x *= s;
            y *= s;
            l *= s;
            a *= s;
            b *= s;
        }
    }
}
