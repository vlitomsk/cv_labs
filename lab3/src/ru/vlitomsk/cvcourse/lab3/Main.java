package ru.vlitomsk.cvcourse.lab3;

import ru.vlitomsk.cvcourse.lab3.controller.labtasks.LabTasksLoader;
import ru.vlitomsk.cvcourse.lab3.view.MainWindow;

public class Main {
    public static void main(String[] args) {
        new MainWindow(LabTasksLoader.Load());
    }
}
